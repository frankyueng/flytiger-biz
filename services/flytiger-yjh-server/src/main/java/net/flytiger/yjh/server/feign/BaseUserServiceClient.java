package net.flytiger.yjh.server.feign;

import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.opencloud.common.model.ResultBody;
import com.opencloud.common.mybatis.base.entity.ResPage;

import io.swagger.annotations.Api;
import net.flytger.yjh.client.model.dto.DemoUserAddDto;
import net.flytger.yjh.client.model.dto.DemoUserDto;
import net.flytger.yjh.client.model.entity.DemoUser;
import net.flytger.yjh.client.service.IDemoServiceClient;
import net.flytiger.yjh.server.service.BaseUserService;

/**
 * 基础用户信息
 *
 * @author mbc
 */
@Api(value = "Fegin基础用户管理", tags = "Fegin基础用户管理")
@RestController
public class BaseUserServiceClient implements IDemoServiceClient {
    @Autowired
    private BaseUserService baseUserService;


    @Override
    public ResultBody addUser(@RequestBody @Valid DemoUserAddDto baseUserAddDto) {
        DemoUser baseUser = baseUserService.addUser(baseUserAddDto);
        return ResultBody.ok().data(baseUser);
    }

    @Override
    public List<DemoUser> getUserByIds(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return Collections.emptyList();
        }
        return baseUserService.listByIds(userIds);
    }

    @Override
    public ResultBody<ResPage<DemoUser>> getUserList(@RequestBody DemoUserDto baseUserDto) {
        List<DemoUser> userList = baseUserService.getUserList(baseUserDto);
        ResPage<DemoUser> page = ResPage.build(userList, baseUserDto);
        return ResultBody.ok(page);
    }


}
