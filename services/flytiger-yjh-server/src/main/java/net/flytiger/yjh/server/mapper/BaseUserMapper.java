package net.flytiger.yjh.server.mapper;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.opencloud.common.mybatis.base.mapper.SuperMapper;

import net.flytger.yjh.client.model.dto.DemoUserDto;
import net.flytger.yjh.client.model.entity.DemoUser;

/**
 * @author liuyadu
 */
@Repository
public interface BaseUserMapper extends SuperMapper<DemoUser> {

    List<DemoUser> getUserList(DemoUserDto baseUserDto);

}
