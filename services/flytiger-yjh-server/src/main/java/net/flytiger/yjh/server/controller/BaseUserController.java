package net.flytiger.yjh.server.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencloud.common.model.ResultBody;
import com.opencloud.common.mybatis.base.entity.ResPage;
import com.opencloud.common.utils.DateUtils;
import com.opencloud.common.utils.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.flytger.yjh.client.model.dto.DemoUserAddDto;
import net.flytger.yjh.client.model.dto.DemoUserDto;
import net.flytger.yjh.client.model.dto.DemoUserUpdateDto;
import net.flytger.yjh.client.model.entity.DemoUser;
import net.flytiger.yjh.server.service.BaseUserService;

/**
 * 基础用户信息
 *
 * @author mbc
 */
@Api(tags = "基础用户管理")
@RestController
@RequestMapping("baseuser")
public class BaseUserController {
    @Autowired
    private BaseUserService baseUserService;

    @ApiOperation(value = "添加基础用户")
    @PostMapping("/addUser")
    public ResultBody<DemoUser> addUser(@RequestBody DemoUserAddDto baseUserAddDto) {
        DemoUser baseUser = baseUserService.addUser(baseUserAddDto);
        return ResultBody.ok().data(baseUser);
    }

    @ApiOperation(value = "分页查询基础用户列表")
    @PostMapping("/getUserList")
    public ResultBody<ResPage<DemoUser>> getUserList(@RequestBody @Valid DemoUserDto baseUserDto) {
        List<DemoUser> userList = baseUserService.getUserList(baseUserDto);
        ResPage<DemoUser> page = ResPage.build(userList, baseUserDto);
        return ResultBody.ok(page);
    }

    @ApiOperation(value = "分页查询基础用户列表 by:mybatis-plus")
    @PostMapping("/getUserListByPlus")
    public ResultBody<ResPage<DemoUser>> getUserListByPlus(@RequestBody @Valid DemoUserDto baseUserDto) {
        IPage<DemoUser> userList = baseUserService.findListPage(baseUserDto);
        ResPage<DemoUser> page = ResPage.build(userList, baseUserDto);
        return ResultBody.ok(page);
    }

    @ApiOperation(value = "更新用户信息", notes = "更新用户信息")
    @PutMapping("/updateUserInfo")
    public ResultBody updateUserInfo(@RequestBody @Valid DemoUserUpdateDto baseUserUpdateDto) {
        DemoUser baseUser = new DemoUser();
        BeanUtils.copyProperties(baseUserUpdateDto, baseUser, "birthday");
        baseUser.setBirthday(StringUtils.isNotBlank(baseUserUpdateDto.getBirthday()) ? DateUtils.parseDate(baseUserUpdateDto.getBirthday()) : null);
        baseUserService.updateUser(baseUser);
        return ResultBody.ok();
    }


    @ApiOperation(value = "删除用户", notes = "删除用户")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", required = true, value = "id", paramType = "form")})
    @DeleteMapping("/deleteUser")
    public ResultBody<DemoUser> deleteUser(@RequestParam(value = "id") Long id) {
        baseUserService.deleteUser(id);
        return ResultBody.ok();
    }

}
