package net.flytiger.yjh.server.service;


import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencloud.common.mybatis.base.service.IBaseService;

import net.flytger.yjh.client.model.dto.DemoUserAddDto;
import net.flytger.yjh.client.model.dto.DemoUserDto;
import net.flytger.yjh.client.model.entity.DemoUser;

/**
 * 基础用户资料管理
 *
 * @author: mbc
 * @date: 2020/04/24 16:38
 * @description:
 */
public interface BaseUserService extends IBaseService<DemoUser> {

    /**
     * 添加系统用户
     */
    DemoUser addUser(DemoUserAddDto baseUserAddDto);

    /**
     * 查询列表
     */
    List<DemoUser> getUserList(DemoUserDto baseUserDto);

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    IPage<DemoUser> findListPage(DemoUserDto query);

    /**
     * 更新系统用户
     */
    void updateUser(DemoUser baseUser);

    /**
     * 删除用户
     */
    void deleteUser(Long id);
}
