package net.flytiger.yjh.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencloud.common.model.ResultBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.flytiger.yjh.server.service.BaseUserService;

/**
 * 基础用户信息
 *
 * @author mbc
 */
@Api(tags = "分销测试接口")
@RestController
@RequestMapping
public class BaseTestController {
    @Autowired
    private BaseUserService baseUserService;

    @ApiOperation(value = "测试1")
    @GetMapping("/demo_test")
    public ResultBody<String> test() {
        return ResultBody.ok().data("测试成功");
    }


}
