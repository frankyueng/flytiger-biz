package net.flytiger.yjh.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 微服务启动类服务
 *
 * @author mbc
 */
@EnableCaching
@EnableFeignClients(basePackages = {"net.flytiger.*.server","com.opencloud.*.client.service"})
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages= {"com.opencloud.datapermit","net.flytiger.yjh.server"})
@MapperScan(basePackages = "net.flytiger.yjh.server.mapper")
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
