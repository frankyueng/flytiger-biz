package net.flytger.yjh.client.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "更新基础用户参数demo")
@Data
public class DemoUserUpdateDto {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "性别: 1男 0女")
    private Integer sex;

    @ApiModelProperty(value = "出生日期")
    private String birthday;

}
