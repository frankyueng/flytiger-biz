package net.flytger.yjh.client.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.opencloud.common.model.ResultBody;
import com.opencloud.common.mybatis.base.entity.ResPage;

import io.swagger.annotations.ApiOperation;
import net.flytger.yjh.client.constants.DemoConstants;
import net.flytger.yjh.client.model.dto.DemoUserAddDto;
import net.flytger.yjh.client.model.dto.DemoUserDto;
import net.flytger.yjh.client.model.entity.DemoUser;

/**
 * @author mbc
 */
@FeignClient(value = DemoConstants.FLYTIGER_DEMO_SERVER)
public interface IDemoServiceClient {

    @ApiOperation(value = "添加基础用户")
    @PostMapping("/api/baseuser/addUser")
    ResultBody<DemoUser> addUser(@RequestBody DemoUserAddDto baseUserAddDto);

    @ApiOperation(value = "通过用户ID集合查找用户")
    @PostMapping("/api/baseuser/getUserByIds")
    List<DemoUser> getUserByIds(@RequestBody List<Long> userIds);

    @ApiOperation(value = "查询基础用户列表")
    @PostMapping("/api/baseuser/getUserList")
    ResultBody<ResPage<DemoUser>> getUserList(@RequestBody DemoUserDto baseUserDto);

}
