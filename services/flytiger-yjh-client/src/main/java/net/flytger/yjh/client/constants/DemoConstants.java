package net.flytger.yjh.client.constants;

/**
 * 用户常量类
 * @author sjl
 */
public class DemoConstants {
    /**
     * 微服务名称
     */
    public static final String FLYTIGER_DEMO_SERVER = "flytiger-yjh-server";

}
