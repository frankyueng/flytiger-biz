package net.flytger.yjh.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.flytger.yjh.client.model.entity.DemoUser;

/**
 * 用户详情VO
 *
 * @author mbc
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserVo extends DemoUser {

    @ApiModelProperty(value = "区域Id数组")
    private String districtIdAry;

    @ApiModelProperty(value = "区域名称数组")
    private String districtNameAry;

}
